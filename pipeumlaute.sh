#!/bin/bash
#filter (pipe) script für mg.mud.de um es sinnvoll per espeak zu verarbeiten

#how to use:
#terminal 1:
##cd somewhere-you-like-to-save-eg:
#cd /tmp
#script -f mg170326
#telnet mg.mud.de

#terminal 2
#tail -f mg170326 |pipeumlaute.sh|espeak -v de -s 200 

sed -e 's/Ae/Ä/g' -e 's/AE/Ä/g' -e 's/ae/ä/g' -e 's/Ue/Ü/g' -e 's/UE/Ü/g' -e 's/ue/ü/g' -e 's/Oe/Ö/g' -e 's/OE/Ö/g' -e 's/oe/ö/g' 
